
// These two elements are full screen wrappers. They are declared because they are used in the code below.
// modal has a styling to take up the full screen with no scrolling
let modal = document.getElementById('project-modal');
// wrapper has a styling to take up the full screen with scrolling
let wrapper = document.getElementById('wrapper');

// If an gallery photo is clicked then open the modal and hide wrapper
document.getElementById('item1').onclick = function() {

    console.log('clicked')
    modal.style.transform = "translate(0, 0)";
    document.body.classList.add('noscroll')

}
document.getElementById('item2').onclick = function() {

    console.log('clicked')
    modal.style.transform = "translate(0, 0)";
    document.body.classList.add('noscroll')

}
// If the close button is clicked then close the modal and show the wrapper
document.getElementById('project-modal__content-close').onclick = function() {
    
    modal.style.transform = "translate(-100%, 0)";
    document.body.classList.remove('noscroll')
}