
// Scroll to Section
let aboutArea = document.getElementsByClassName("about");
let portfolioArea = document.getElementsByClassName("portfolio");
let skillsArea = document.getElementsByClassName("skills");
let jobsArea = document.getElementsByClassName("jobs");

// Hero Button
let heroBtn = document.getElementById("hero__btn-portfolio");

// Header Nav
let portfolioNav = document.getElementById("header__nav-portfolio");
let aboutNav = document.getElementById("header__nav-about");
let skillsNav = document.getElementById("header__nav-skills");
let jobsNav = document.getElementById("header__nav-jobs");

// Mobile Nav
let mobilePortfolioNav = document.getElementById("mobile-nav__nav-portfolio");
let mobileAboutNav = document.getElementById("mobile-nav__nav-about");
let mobileSkillsNav = document.getElementById("mobile-nav__nav-skills");
let mobileJobsNav = document.getElementById("mobile-nav__nav-jobs");



// Scroll to portfolio section
scrollToPortfolio = function () {
  window.scrollTo({
    top: portfolioArea[0].offsetTop,
    behavior: "smooth",
  });
};

// Scroll to about section
scrollToAbout = function () {
  window.scrollTo({
    top: aboutArea[0].offsetTop,
    behavior: "smooth",
  });
};


// Scroll to skills section
scrollToSkills = function () {
  window.scrollTo({
    top: skillsArea[0].offsetTop,
    behavior: "smooth",
  });
};

// Scroll to jobs section
scrollToJobs = function () {
  window.scrollTo({
    top: jobsArea[0].offsetTop,
    behavior: "smooth",
  });
};

// Scroll to Section End
