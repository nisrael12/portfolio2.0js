// Contentful Hero Content
// getEntries is a Contentful method that will return an array of entries.
client
	.getEntries({
		content_type: 'hero'
	})
	.then(function (entries) {
		entries.items.forEach(function (entry) {
			let signature = (entry.fields.fullName);
			let position = (entry.fields.position);
			let otherPositions = (entry.fields.otherPositions);
			let btnText = (entry.fields.buttonText);
			let backgroundPhoto = (entry.fields.backgroundPhoto.fields.file.url);
			document.getElementsByClassName('hero__signature')[0].innerHTML = signature;
			document.getElementsByClassName('hero__position-content')[0].innerHTML = position;
			document.getElementsByClassName('hero__titles-content')[0].innerHTML = otherPositions[0];
			document.getElementsByClassName('hero__titles-content')[1].innerHTML = otherPositions[1];
			document.getElementsByClassName('hero__titles-content')[2].innerHTML = otherPositions[2];
			document.getElementsByClassName('hero__btn-text')[0].innerHTML = btnText;
			document.getElementsByClassName('hero')[0].style.backgroundImage = `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)) url(${backgroundPhoto})`;
		})
	});

