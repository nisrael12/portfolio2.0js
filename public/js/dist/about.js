client
	.getEntries({
		content_type: 'about'
	})
	.then(function (entries) {
		entries.items.forEach(function (entry) {
			let header = (entry.fields.aboutMeHeader);
			let summary = (entry.fields.aboutSummary);
			let name = (entry.fields.name);
			let email = (entry.fields.email);
			let phone = (entry.fields.phone);
			let nation = (entry.fields.nation);
			let city = (entry.fields.city);
			let state = (entry.fields.state);
			let resume = (entry.fields.resume.fields.file.url);
			let portraitImage = (entry.fields.portraitImage.fields.file.url);
			let aboutResume = document.getElementsByClassName('about__resume')[0];

			document.getElementsByClassName('about__content-answer')[0].innerHTML = name;
			document.getElementsByClassName('about__content-answer')[1].innerHTML = email;
			document.getElementsByClassName('about__content-answer')[2].innerHTML = phone;
			document.getElementsByClassName('about__content-answer')[3].innerHTML = nation;
			document.getElementsByClassName('about__content-answer')[4].innerHTML = city;
			document.getElementsByClassName('about__content-answer')[5].innerHTML = state;
			document.getElementsByClassName('about__img-input')[0].src = portraitImage;
			aboutResume.setAttribute('href', resume);
			
		})
	});