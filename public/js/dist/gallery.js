
filterSelection("all") // Execute the function and show all columns
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("portfolio__grid-item");
  if (c == "all") c = "";
  // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

// Show filtered elements
function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}

// Hide elements that are not selected
function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}

// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("portfolio__gallery-filter");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}



// // Sample Data
let images = [ {name: 'plane', url: '\img\/1.jpg'}, {name:'lady', url: '\img\/2.jpg'}, {name: 'man', url:'\img\/3.jpg'}];


// Declare the parent container
var portfolioGrid = document.getElementsByClassName('portfolio__grid')[0]

// This function creates an img node with the data
function createImageNode(imgSrc) {
  var img = document.createElement('img')
  img.src = imgSrc
  return img
}


function photo(entryId){
  return client.getEntry(entryId).then(function (entry) {
     // logs the entry metadata
     let contentImage = (entry.fields.galleryImage[0].fields.file.url);
     // logs the field with ID title
     return contentImage
 })}


// Sid help wrote this async functions to pull in the promise from contentful. 
 const prepImages = async () => {
  images[0].url = await photo('BrHH5Tur3IOekvxntAOYx')
  images[1].url = await photo('ZlW44ratdG4CDfw3rS5CI')
  images[2].url = await photo('54KkOJOxhXLvhi0zM3x0iZ')

  images.forEach(img => {
    portfolioGrid.appendChild(createImageNode(img.url))
  });
  };
  // images[0].url = await photo('BrHH5Tur3IOekvxntAOYx')

  // Loop through the Sample Data and create an img node for each item
// images.forEach(img => {
//   portfolioGrid.appendChild(createImageNode(img.url))
// });

prepImages()




 // Loop through gallery items and add click event listeners to each one
 // Create a slide show function that will show the next image in the gallery and repeat