import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Layout extends Component {
	constructor() {
		super();
		this.state = {
			name: 'Joe'
		};
	}
	clickedBtn = () => {};
	async test() {}
	render() {
		return (
			<div id="app">
				<div class="home">
					<div class="Aligner">
						<div class="Aligner-item">
							<h1>React Activated</h1>
							<div class="menu">
								<ul>
									<li>Navi Israel</li>
								</ul>
							</div>
							<div class="version-num">version 4.0.0</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const app = document.getElementById('app');

ReactDOM.render(<Layout />, app);
